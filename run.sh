#!/bin/bash

if [ "$1" != "start" ] && [ "$1" != "restart" ] ; then
    echo "start|restart"
    exit;
fi

sudo forever "$1" -o out.log -e err.log "`pwd`/gitsync.js"