var http = require('http'),
    qs = require('querystring'),
    fs = require('fs'),
    exec = require('child_process').exec,
    async = require('async'),
    ini = require('ini');

var config;

try {
    config = ini.parse(fs.readFileSync('/etc/gitsync.ini', 'utf-8'));
} catch (e) {
    return console.log("Fail to read /etc/gitsync.ini. Error: " + e.message);
}


console.log("Watching to sync in " + config.path + " from branch " + config.branch);

var responseError = function(response) {
    response.writeHead(500);
    response.write('Error');
    return response.end();
};

var command = function( cmd ) {
    return function( callback ) {

    }
}

var sync = function(payload, response) {

    if (payload.commits && payload.commits.length) {

        //take las commit in case we have more from pull requests
        var commit = payload.commits[payload.commits.length - 1];

        //check branch of that commit
        if (commit.branch != config.branch) {
            return console.log("Skip commit, not on branch " + config.branch);
        }

        var command = 'cd ' + config.path + ' && ' +
            'git reset --hard && ' +
            'git clean -f  && ' +
            'git pull origin '+commit.branch+' -q && ' + 
            'rm -rf app/cache/*';

        exec(command, function(error, out, stderror) {

            if (error) {
                return console.log("Checkout error: " + error);
            }

            if (stderror) {
                return console.log("STDError: " + stderror);
            }

            console.log("Sync " + commit.raw_node + " complete from " + commit.user + " at " + commit.timestamp);

            console.log("Build ng...");

            exec('cd ' + config.path + "/web/ng && bower install && grunt build", function(error, out, stderror) {

                if( error ) {
                    return console.log("Build ng dist error:", error);
                    }
            
                if( stderror ) {
                    return console.log("STDError:", stderror);
                }

                console.log("Ng build complete!");
            });
            
        });


        response.writeHead(200);
        response.write('OK');
        return response.end();
    }

    console.log("No commits:", payload.commits);

    response.writeHead(500);
    response.end();
};

var server = http.createServer(function(request, response) {

    if (request.url === '/favicon.ico') {
        response.writeHead(404);
        return;
    }

    if (request.url === '/status') {

        exec('cd ' + config.path + " && git log -n 1", function(error, out, stderr) {
            
            if (error || stderr) {
                return responseError(response);
            }

            var hash = '',
                date = '';

            var parts = out.split("\n");

            for( var i=0; i<parts.length; i++){
                
                if( parts[i].indexOf('commit') === 0 ) {
                    hash = parts[i].substr(7);
                }
                
                if( parts[i].indexOf('Date') === 0 ) {
                    date = parts[i].substr(8);
                }

            }

            response.writeHead(200);
            response.write(hash + ' @ ' + date);
            return response.end();
        });

        return;
    }

    var body = '';

    request.on('data', function(chunk) {

        body += chunk;

        if (body.length > 1e6) {
            request.connection.destroy();
        }
    });

    request.on('end', function() {

        var post = qs.parse(body);

        if (typeof(post['payload']) !== 'undefined') {
            sync(JSON.parse(post['payload']), response);
        } else {
            console.log('No post data', post);
            response.writeHead(500);
            response.end();
        }
    });
});

var port = 8000;

server.listen(port, function() {
    console.log("Listining on " + port);
});